# Rili Socket service

This is module of [rili](https://gitlab.com/rilis/rili) which contain implementation of basic network primitives

**Documentation and API reference** :  [rilis.io](https://rilis.io/projects/rili/library/service/socket)

**[Issue tracker](https://gitlab.com/rilis/rilis/issues)**

**Project status**

[![build status](https://gitlab.com/rilis/rili/service/socket/badges/master/build.svg)](https://gitlab.com/rilis/rili/service/socket/commits/master)
[![coverage report](https://gitlab.com/rilis/rili/service/socket/badges/master/coverage.svg)](https://gitlab.com/rilis/rili/service/socket/commits/master)
