#include <rili/Test.hpp>
#include <rili/service/Socket.hpp>
#include <rili/service/SocketConfig.hxx>
#include <string>
#include <thread>
#include <vector>

TEST(Socket, ErrorMessages) {
    EXPECT_EQ(
        rili::service::Socket::Connection::ReadableWouldBlock().what(),
        std::string(
            "rili::service::SocketBase::Connection::ReadableWouldBlock: operation not completed succesfully because "
            "non-blocking read operation would block otherwise. Try again."));
    EXPECT_EQ(
        rili::service::Socket::Connection::WritableWouldBlock().what(),
        std::string(
            "rili::service::SocketBase::Connection::WritableWouldBlock: operation not completed succesfully because "
            "non-blocking write operation would block otherwise. Try again."));
    EXPECT_EQ(rili::service::Socket::CreateConnectionConnectError().what(),
              std::string("rili::service::SocketBase::CreateConnectionConnectError: Cannot connect to address"));
    EXPECT_EQ(rili::service::Socket::CreateConnectionResolveError().what(),
              std::string("rili::service::SocketBase::CreateConnectionResolveError: Cannot resolve address"));
    EXPECT_EQ(rili::service::Socket::CreateAcceptorBindError().what(),
              std::string("rili::service::SocketBase::CreateAcceptorBindError: Cannot bind to address"));
    EXPECT_EQ(rili::service::Socket::CreateAcceptorResolveError().what(),
              std::string("rili::service::SocketBase::CreateAcceptorResolveError: Cannot resolve address"));

    EXPECT_FALSE(
        std::string(rili::service::Socket::Acceptor::Error(std::error_code(ENOSYS, std::generic_category())).what())
            .empty());
}

TEST(Socket, LocalSingleCharacterSendReceive) {
    auto& socket = rili::service::Socket::instance();
    auto acceptor = socket.createAcceptor("0.0.0.0", 12345);
    auto connection1 = socket.createConnection("127.0.0.1", 12345);
    auto connection2 = acceptor->accept();

    EXPECT_EQ(connection1->mode(), rili::service::Socket::Connection::Mode::Blocking);
    EXPECT_EQ(connection2->mode(), rili::service::Socket::Connection::Mode::Blocking);
    EXPECT_TRUE(*connection1 << 's' << rili::stream::flush);
    EXPECT_TRUE(*connection2 << 'e' << rili::stream::flush);

    char resp1;
    char resp2;

    auto result1 = *connection1 >> resp1;
    auto result2 = *connection2 >> resp2;

    EXPECT_TRUE(result1);
    EXPECT_TRUE(result2);

    EXPECT_EQ(resp1, 'e');
    EXPECT_EQ(resp2, 's');
}

TEST(Socket, LocalRequestResponse) {
    auto& socket = rili::service::Socket::instance();
    auto acceptor = socket.createAcceptor("0.0.0.0", 12345);
    auto connection1 = socket.createConnection("127.0.0.1", 12345);
    auto connection2 = acceptor->accept();

    EXPECT_EQ(connection1->mode(), rili::service::Socket::Connection::Mode::Blocking);
    EXPECT_EQ(connection2->mode(), rili::service::Socket::Connection::Mode::Blocking);
    EXPECT_TRUE(*connection1 << "something " << rili::stream::flush);
    EXPECT_TRUE(*connection2 << "else " << rili::stream::flush);

    std::string resp1;
    std::string resp2;

    auto result1 = *connection1 >> resp1;
    auto result2 = *connection2 >> resp2;

    EXPECT_TRUE(result1);
    EXPECT_TRUE(result2);

    EXPECT_EQ(resp1, "else");
    EXPECT_EQ(resp2, "something");
}

TEST(Socket, WhenRemoteClosed) {
    auto& socket = rili::service::Socket::instance();
    auto acceptor = socket.createAcceptor("0.0.0.0", 12345);
    auto connection1 = socket.createConnection("127.0.0.1", 12345);
    auto connection2 = acceptor->accept();

    EXPECT_TRUE(*connection1 << "some" << rili::stream::flush);
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    connection2.reset(nullptr);
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    EXPECT_FALSE(*connection1 << "some" << rili::stream::flush);
}

TEST(Socket, WhenCreateConnectionToNotExistingHost) {
    auto& socket = rili::service::Socket::instance();
    EXPECT_THROW(rili::service::SocketBase::CreateConnectionResolveError,
                 [&socket]() { socket.createConnection("not-existing.rilis.io", 80); });
}

TEST(Socket, WhenCreateConnectionToNotExistingPort) {
    auto& socket = rili::service::Socket::instance();
    EXPECT_THROW(rili::service::SocketBase::CreateConnectionConnectError,
                 [&socket]() { socket.createConnection("hell.rilis.io", 65535); });
}

TEST(Socket, WhenCreateAcceptorToNotExistingHost) {
    auto& socket = rili::service::Socket::instance();
    EXPECT_THROW(rili::service::SocketBase::CreateAcceptorResolveError,
                 [&socket]() { socket.createAcceptor("not-existing.rilis.io", 80); });
}

TEST(Socket, WhenCreateAcceptorToReservedPort) {
    auto& socket = rili::service::Socket::instance();
    auto acceptor = socket.createAcceptor("0.0.0.0", 12345);
    EXPECT_THROW(rili::service::SocketBase::CreateAcceptorBindError,
                 [&socket]() { socket.createAcceptor("0.0.0.0", 12345); });
}

TEST(Socket, WhenCreateAcceptorToNonLocalHost) {
    auto& socket = rili::service::Socket::instance();
    EXPECT_THROW(rili::service::SocketBase::CreateAcceptorBindError,
                 [&socket]() { socket.createAcceptor("gitlab.com", 12345); });
}

TEST(Socket, WhenConnectionClosedBeforeAccept) {
    auto& socket = rili::service::Socket::instance();
    auto acceptor = socket.createAcceptor("0.0.0.0", 12345);
    { socket.createConnection("127.0.0.1", 12345); }
    auto connection = acceptor->accept();
    char i;
    EXPECT_FALSE(*connection >> i);
}

TEST(Socket, LocalNonBlockingRequestResponseWhenNothingBlocks) {
    auto& socket = rili::service::Socket::instance();
    auto acceptor = socket.createAcceptor("0.0.0.0", 12345);
    auto connection1 = socket.createConnection("127.0.0.1", 12345);
    auto connection2 = acceptor->accept();

    rili::service::Socket::Connection::ModeGuard guard1(*connection1,
                                                        rili::service::Socket::Connection::Mode::NonBlocking);
    rili::service::Socket::Connection::ModeGuard guard2(*connection2,
                                                        rili::service::Socket::Connection::Mode::NonBlocking);

    EXPECT_EQ(connection1->mode(), rili::service::Socket::Connection::Mode::NonBlocking);
    EXPECT_EQ(connection2->mode(), rili::service::Socket::Connection::Mode::NonBlocking);

    EXPECT_TRUE(*connection1 << "something " << rili::stream::flush);
    std::string resp;
    std::this_thread::sleep_for(std::chrono::milliseconds(5));
    auto result = *connection2 >> resp;
    EXPECT_TRUE(result);
    EXPECT_EQ(resp, "something");
}

TEST(Socket, LocalNonBlockingWhenNoDataToReceive) {
    auto& socket = rili::service::Socket::instance();
    auto acceptor = socket.createAcceptor("0.0.0.0", 12345);
    auto connection1 = socket.createConnection("127.0.0.1", 12345);
    auto connection2 = acceptor->accept();

    rili::service::Socket::Connection::ModeGuard guard1(*connection1,
                                                        rili::service::Socket::Connection::Mode::NonBlocking);
    rili::service::Socket::Connection::ModeGuard guard2(*connection2,
                                                        rili::service::Socket::Connection::Mode::NonBlocking);

    {
        std::string resp1;
        EXPECT_FALSE(connection1->readWouldBlock());
        auto result1 = *connection1 >> resp1;
        EXPECT_FALSE(result1);
        EXPECT_TRUE(connection1->readWouldBlock());
    }

    {
        std::string resp2;
        EXPECT_FALSE(connection2->readWouldBlock());
        auto result2 = *connection2 >> resp2;
        EXPECT_FALSE(result2);
        EXPECT_TRUE(connection2->readWouldBlock());
    }
}
#ifndef RILI_HAVE_WINSOCK  // winsock will accept huge amounts of data and rather crash than say WSAEWOULDBLOCK for send
TEST(Socket, LocalNonBlockingWhenBigDataToSend) {
    auto& socket = rili::service::Socket::instance();
    auto connection1 = socket.createConnection("rilis.io", 80);

    rili::service::Socket::Connection::ModeGuard guard1(*connection1,
                                                        rili::service::Socket::Connection::Mode::NonBlocking);

    EXPECT_EQ(connection1->mode(), rili::service::Socket::Connection::Mode::NonBlocking);
    {
        std::vector<char> buf(1024 * 1024 * 5);
        connection1->write(buf.data(), buf.size());
    }
    EXPECT_FALSE(*connection1 << rili::stream::flush);
    EXPECT_TRUE(connection1->writeWouldBlock());
    EXPECT_LT(connection1->writableView().size(), 1024u * 1024u * 5);
}
#endif
