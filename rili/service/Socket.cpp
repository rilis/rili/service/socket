#include <rili/service/SocketConfig.hxx>

#ifdef RILI_HAVE_WINSOCK2_H
#include <winsock2.h>
#endif
#ifdef RILI_HAVE_WS2TCPIP_H
#include <ws2tcpip.h>
#endif
#ifdef RILI_HAVE_WINDOWS_H
#include <windows.h>
#endif

#ifdef RILI_HAVE_FCNTL_H
#include <fcntl.h>
#endif
#ifdef RILI_HAVE_NETDB_H
#include <netdb.h>
#endif
#ifdef RILI_HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif
#ifdef RILI_HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif
#ifdef RILI_HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif
#ifdef RILI_HAVE_UNISTD_H
#include <unistd.h>
#endif

#include <algorithm>
#include <cstdint>
#include <cstring>
#include <memory>
#include <rili/MakeUnique.hpp>
#include <rili/service/Socket.hpp>
#include <string>
#include <system_error>

#ifdef RILI_HAVE_WINSOCK
typedef SOCKET SocketType;
typedef int LenType;
SocketType constexpr INVALID_SOCKET_VALUE = INVALID_SOCKET;
#else
typedef int SocketType;
typedef socklen_t LenType;
SocketType constexpr INVALID_SOCKET_VALUE = -1;
#endif

namespace rili {
namespace service {

const char* SocketBase::Connection::ReadableWouldBlock::what() const noexcept {
    static char const* msg =
        "rili::service::SocketBase::Connection::ReadableWouldBlock: operation not completed succesfully because "
        "non-"
        "blocking read operation would block otherwise. Try again.";
    return msg;
}

const char* SocketBase::Connection::WritableWouldBlock::what() const noexcept {
    static char const* msg =
        "rili::service::SocketBase::Connection::WritableWouldBlock: operation not completed succesfully because "
        "non-"
        "blocking write operation would block otherwise. Try again.";
    return msg;
}

const char* SocketBase::CreateConnectionConnectError::what() const noexcept {
    static char const* msg = "rili::service::SocketBase::CreateConnectionConnectError: Cannot connect to address";
    return msg;
}

const char* SocketBase::CreateConnectionResolveError::what() const noexcept {
    static char const* msg = "rili::service::SocketBase::CreateConnectionResolveError: Cannot resolve address";
    return msg;
}

const char* SocketBase::CreateAcceptorBindError::what() const noexcept {
    static char const* msg = "rili::service::SocketBase::CreateAcceptorBindError: Cannot bind to address";
    return msg;
}

const char* SocketBase::CreateAcceptorResolveError::what() const noexcept {
    static char const* msg = "rili::service::SocketBase::CreateAcceptorResolveError: Cannot resolve address";
    return msg;
}

namespace {
#ifdef RILI_HAVE_MSG_NOSIGNAL
auto constexpr msgNoSignalOption = MSG_NOSIGNAL;
#else
auto constexpr msgNoSignalOption = 0;
#endif

#ifdef RILI_HAVE_WINSOCK
int setsockoptWrapper(SocketType s, int level, int optname, void* opt, std::size_t optsize) {
    return ::setsockopt(s, level, optname, reinterpret_cast<const char*>(opt), static_cast<int>(optsize));
}
bool nonBlockingWouldBlock() { return WSAGetLastError() == WSAEWOULDBLOCK; }
#else
int setsockoptWrapper(SocketType s, int level, int optname, void* opt, std::size_t optsize) {
    return ::setsockopt(s, level, optname, opt, static_cast<socklen_t>(optsize));
}
bool nonBlockingWouldBlock() { return errno == EWOULDBLOCK || errno == EAGAIN; }
#endif

class ConnectionImpl : public SocketBase::Connection {
 public:
    ConnectionImpl() = delete;
    ConnectionImpl(ConnectionImpl const&) = delete;
    ConnectionImpl& operator=(ConnectionImpl const&) = delete;

    explicit ConnectionImpl(SocketType sockFd)
        : SocketBase::Connection(),
          m_readView(),
          m_writeView(),
          m_sockFd(sockFd),
          m_readWouldBlock(false),
          m_writeWouldBlock(false),
          m_mode(Mode::NonBlocking) {
        mode(Mode::Blocking);
    }

    ConnectionImpl(const std::string& host, std::uint16_t port)
        : SocketBase::Connection(),
          m_readView(),
          m_writeView(),
          m_sockFd(INVALID_SOCKET_VALUE),
          m_readWouldBlock(true),
          m_writeWouldBlock(true),
          m_mode(Mode::Blocking) {
        struct addrinfo hints, *servinfo, *p;
        int rv;
        std::memset(&hints, 0, sizeof(hints));
        hints.ai_family = AF_UNSPEC;
        hints.ai_socktype = SOCK_STREAM;

        if ((rv = ::getaddrinfo(host.c_str(),
                                std::to_string(static_cast<unsigned int>(port)).c_str(),  // NOLINT (runtime/int)
                                &hints, &servinfo)) != 0) {
            throw SocketBase::CreateConnectionResolveError();
        }

        for (p = servinfo; p != NULL; p = p->ai_next) {
            if ((m_sockFd = ::socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == INVALID_SOCKET_VALUE) {
                continue;
            }

            if (::connect(m_sockFd, p->ai_addr, static_cast<LenType>(p->ai_addrlen)) == -1) {
                close();
                continue;
            }

#ifdef RILI_HAVE_SO_NOSIGPIPE
            int yes = 1;
            if (setsockoptWrapper(m_sockFd, SOL_SOCKET, SO_NOSIGPIPE, &yes, sizeof(int)) == -1) {
                close();
                continue;
            }
#endif
            break;
        }

        ::freeaddrinfo(servinfo);

        if (p == nullptr || m_sockFd == INVALID_SOCKET_VALUE) {
            throw SocketBase::CreateConnectionConnectError();
        }
    }

    void close() {
        if (m_sockFd != INVALID_SOCKET_VALUE) {
#ifdef RILI_HAVE_WINSOCK
            ::closesocket(m_sockFd);
#else
            ::close(m_sockFd);
#endif
            m_sockFd = INVALID_SOCKET_VALUE;
        }
    }
    virtual ~ConnectionImpl() { close(); }

 public:
    void consume(std::size_t size) override {
        m_readView.raw().erase(
            m_readView.raw().begin(),
            std::next(m_readView.raw().begin(),
                      static_cast<rili::stream::view::Default::container_type::difference_type>(size)));
    }
    stream::view::Base const& readableView() const override { return m_readView; }
    stream::view::Base const& writableView() const override { return m_writeView; }
    void write(char const* data, std::size_t size) override {
        m_writeView.raw().insert(m_writeView.raw().size(), data, size);
    }

    std::exception_ptr flush() override {
        if (!m_writeView.empty()) {
            if (m_mode == Mode::NonBlocking) {
                m_writeWouldBlock = false;
            }

            char const* buf = m_writeView.data();
            std::size_t len = m_writeView.size();
            std::size_t total = 0;
            std::size_t bytesleft = len;
            std::int64_t n = 0;

            std::exception_ptr eptr;

            while (total < len) {
                n = ::send(m_sockFd, buf + total, static_cast<LenType>(bytesleft), msgNoSignalOption);
                if (n <= 0) {
                    if (m_mode == Mode::NonBlocking && nonBlockingWouldBlock()) {
                        m_writeWouldBlock = true;
                        eptr = std::make_exception_ptr(WritableWouldBlock());
                        break;
                    } else if (n < 0) {
                        break;
                    }
                } else {
                    total += static_cast<std::size_t>(n);
                    bytesleft -= static_cast<std::size_t>(n);
                }
            }

            m_writeView.raw().erase(m_writeView.raw().begin(),
                                    std::next(m_writeView.raw().begin(),
                                              static_cast<rili::stream::view::Default::container_type::difference_type>(
                                                  m_writeView.size() - static_cast<std::size_t>(bytesleft))));
            if (bytesleft != 0 && !eptr) {
                eptr = std::make_exception_ptr(stream::error::WritableEnd());
            }
            return eptr;
        } else {
            return {};
        }
    }

    std::exception_ptr receiveSome() {
        char buf[256];

        std::int64_t received = ::recv(m_sockFd, buf, sizeof(buf), msgNoSignalOption);
        if (received <= 0) {
            if (received == 0 || m_mode == Mode::Blocking ||
                (m_mode == Mode::NonBlocking && !nonBlockingWouldBlock())) {
                return std::make_exception_ptr(stream::error::ReadableEnd());
            } else {
                m_readWouldBlock = true;
                return std::make_exception_ptr(ReadableWouldBlock());
            }
        } else {
            m_readView.raw().insert(m_readView.raw().size(), buf, static_cast<std::size_t>(received));
            return {};
        }
    }

    std::exception_ptr pull(std::size_t count) override {
        if (m_mode == Mode::NonBlocking) {
            m_writeWouldBlock = false;
        }

        if (count == 0) {
            return receiveSome();
        } else {
            std::size_t minimalSize = m_readView.size() + count;
            while (m_readView.size() < minimalSize) {
                auto result = receiveSome();
                if (result) {
                    return result;
                }
            }
            return {};
        }
    }

    bool readWouldBlock() const override { return m_readWouldBlock; }
    bool writeWouldBlock() const override { return m_writeWouldBlock; }

    void mode(Mode mode) override {
        if (m_mode != mode) {
#ifdef RILI_HAVE_WINSOCK
            u_long iMode = (mode == Mode::NonBlocking ? 1 : 0);
            int result = ::ioctlsocket(m_sockFd, FIONBIO, &iMode);
            if (result == NO_ERROR) {
                m_mode = mode;
                if (mode == Mode::NonBlocking) {
                    m_readWouldBlock = m_writeWouldBlock = false;
                } else {
                    m_readWouldBlock = m_writeWouldBlock = true;
                }
            }
#else
            int oldfl;
            oldfl = ::fcntl(m_sockFd, F_GETFL);
            if (oldfl != -1 && ((mode == Mode::NonBlocking && -1 != ::fcntl(m_sockFd, F_SETFL, oldfl | O_NONBLOCK)) ||
                                (mode == Mode::Blocking && -1 != ::fcntl(m_sockFd, F_SETFL, oldfl & ~O_NONBLOCK)))) {
                m_mode = mode;
                if (mode == Mode::NonBlocking) {
                    m_readWouldBlock = m_writeWouldBlock = false;
                } else {
                    m_readWouldBlock = m_writeWouldBlock = true;
                }
            }
#endif
        }
    }

    Mode mode() const override { return m_mode; }

 private:
    rili::stream::view::Default m_readView;
    rili::stream::view::Default m_writeView;
    SocketType m_sockFd;
    bool m_readWouldBlock;
    bool m_writeWouldBlock;
    Mode m_mode;
};

class AcceptorImpl : public SocketBase::Acceptor {
 public:
    AcceptorImpl() = delete;
    AcceptorImpl(AcceptorImpl const&) = delete;
    AcceptorImpl& operator=(AcceptorImpl const&) = delete;
    ~AcceptorImpl() { close(); }

 public:
    void close() {
        if (m_sockFd != INVALID_SOCKET_VALUE) {
#ifdef RILI_HAVE_WINSOCK
            ::closesocket(m_sockFd);
#else
            ::close(m_sockFd);
#endif
            m_sockFd = INVALID_SOCKET_VALUE;
        }
    }

    AcceptorImpl(const std::string& host, std::uint16_t port) : SocketBase::Acceptor(), m_sockFd(INVALID_SOCKET_VALUE) {
        struct addrinfo hints, *servinfo, *p;
        int rv;
        int yes;
        std::memset(&hints, 0, sizeof(hints));
        hints.ai_family = AF_UNSPEC;
        hints.ai_socktype = SOCK_STREAM;

        if ((rv = ::getaddrinfo(host.c_str(),
                                std::to_string(static_cast<unsigned int>(port)).c_str(),  // NOLINT (runtime/int)
                                &hints, &servinfo)) != 0) {
            throw SocketBase::CreateAcceptorResolveError();
        }

        for (p = servinfo; p != NULL; p = p->ai_next) {
            yes = 1;
            if ((m_sockFd = ::socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == INVALID_SOCKET_VALUE) {
                continue;
            }
#ifdef RILI_HAVE_WINSOCK
            if (setsockoptWrapper(m_sockFd, SOL_SOCKET, SO_EXCLUSIVEADDRUSE, &yes, sizeof(int)) == -1) {
                close();
                continue;
            }
#else
            if (setsockoptWrapper(m_sockFd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
                close();
                continue;
            }
#endif

#ifdef RILI_HAVE_SO_NOSIGPIPE
            if (setsockoptWrapper(m_sockFd, SOL_SOCKET, SO_NOSIGPIPE, &yes, sizeof(int)) == -1) {
                close();
                continue;
            }
#endif

            if (::bind(m_sockFd, p->ai_addr, static_cast<LenType>(p->ai_addrlen)) != 0) {
                close();
                continue;
            }

            if (::listen(m_sockFd, 10) != 0) {
                close();
                continue;
            }

            break;
        }

        ::freeaddrinfo(servinfo);

        if (p == nullptr || m_sockFd == INVALID_SOCKET_VALUE) {
            throw SocketBase::CreateAcceptorBindError();
        }
    }
    std::unique_ptr<SocketBase::Connection> accept() override {
        struct sockaddr_storage their_addr;
        socklen_t addr_size = sizeof their_addr;
        auto connectionFd = ::accept(m_sockFd, reinterpret_cast<sockaddr*>(&their_addr), &addr_size);
        if (connectionFd != INVALID_SOCKET_VALUE) {
            return rili::make_unique<ConnectionImpl>(connectionFd);
        } else {
            throw Acceptor::Error(std::error_code(errno, std::generic_category()));
        }
    }

 private:
    SocketType m_sockFd;
};
}  // namespace

SocketBase& Socket::instance() {
    static Socket instance;
    return instance;
}

std::unique_ptr<SocketBase::Connection> Socket::createConnection(const std::string& host, std::uint16_t port) {
    return rili::make_unique<ConnectionImpl>(host, port);
}

std::unique_ptr<SocketBase::Connection> Socket::createConnection(int connectedSocket) {
    if (connectedSocket >= 0) {
        return rili::make_unique<ConnectionImpl>(static_cast<SocketType>(connectedSocket));
    } else {
        return nullptr;
    }
}

std::unique_ptr<SocketBase::Acceptor> Socket::createAcceptor(const std::string& host, std::uint16_t port) {
    return rili::make_unique<AcceptorImpl>(host, port);
}

Socket::~Socket() {
#ifdef RILI_HAVE_WINSOCK
    ::WSACleanup();
#endif
}

Socket::Socket() {
#ifdef RILI_HAVE_WINSOCK
    WSAData wsa_data;
    WORD wsa_version = MAKEWORD(2, 2);
    if (::WSAStartup(wsa_version, &wsa_data) != 0) {
        throw std::system_error(ENOTSUP, std::generic_category());
    }
    if (LOBYTE(wsa_data.wVersion) != 2 || HIBYTE(wsa_data.wVersion) != 2) {
        ::WSACleanup();
        throw std::system_error(ENOTSUP, std::generic_category());
    }
#endif
}

}  // namespace service
}  // namespace rili
