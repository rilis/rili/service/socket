#pragma once
#include <memory>
#include <rili/Stream.hpp>
#include <string>

namespace rili {
namespace service {
/**
 * @brief The SocketBase class is abstract interface for service implementations which provide simple TCP/IP networking
 * API
 */
class SocketBase {
 public:
    class CreateConnectionError : public std::exception {};

    /**
     * @brief The CreateConnectionConnectError class is error type which should be used in case when during creating
     * Connection is problem with socket association after correctly resolved dns address
     */
    class CreateConnectionConnectError : public CreateConnectionError {
     public:
        /**
         * @brief what
         * @return
         */
        const char* what() const noexcept override;
    };

    /**
     * @brief The CreateConnectionResolveError class is error type which should be used in case when dns resolver cannot
     * resolve address during creating Connection
     */
    class CreateConnectionResolveError : public CreateConnectionError {
     public:
        /**
         * @brief what
         * @return
         */
        const char* what() const noexcept override;
    };

    class CreateAcceptorError : public std::exception {};

    /**
     * @brief The CreateAcceptorBindError class is error type which should be used in case when during creating
     * Acceptor is problem with socket association after correctly resolved dns address
     */
    class CreateAcceptorBindError : public CreateAcceptorError {
     public:
        /**
         * @brief what
         * @return
         */
        const char* what() const noexcept override;
    };

    /**
     * @brief The CreateAcceptorResolveError class is error type which should be used in case when dns resolver cannot
     * resolve address during creating Acceptor
     */
    class CreateAcceptorResolveError : public CreateAcceptorError {
     public:
        /**
         * @brief what
         * @return
         */
        const char* what() const noexcept override;
    };

    /**
     * @brief The Connection class is stream oriented abstract base interface for duplex TCP/IP connection
     * implementations
     */
    class Connection : public rili::stream::Duplex {
     public:
        /**
         * @brief The Mode enum is used to distinguish blocking and non-blocking modes of Connection
         */
        enum class Mode : bool { Blocking, NonBlocking };

        /**
         * @brief The ModeGuard is RAII style class which set given Mode on given Connection in it's lifetime scope.
         */
        class ModeGuard {
         public:
            /**
             * @brief ModeGuard store current mode and set new one on connection
             * @param connection - connection on which set mode
             * @param mode - required mode
             */
            inline ModeGuard(Connection& connection, Mode mode)
                : m_connection(connection), m_oldMode(m_connection.mode()) {
                m_connection.mode(mode);
            }

            /**
             * @brief ~ModeGuard set connection mode back to old one
             */
            inline ~ModeGuard() { m_connection.mode(m_oldMode); }

         private:
            Connection& m_connection;
            Mode m_oldMode;
        };

        /**
         * @brief The ReadableWouldBlock class is error type which should be used in non-blocking connection mode when
         * read operation fail, but if connection was in blocking mode read oparation would block instead.
         */
        class ReadableWouldBlock : public stream::error::ReadableEnd {
         public:
            /**
             * @brief what
             * @return
             */
            const char* what() const noexcept override;
        };

        /**
         * @brief The WritableWouldBlock class is error type which should be used in non-blocking connection mode when
         * write operation fail, but if connection was in blocking mode write oparation would block instead.
         */
        class WritableWouldBlock : public stream::error::WritableEnd {
         public:
            /**
             * @brief what
             * @return
             */
            const char* what() const noexcept override;
        };

        virtual ~Connection() = default;

        /**
         * @brief mode reconfigure connection to use given Mode
         * @param mode - Mode to set
         */
        virtual void mode(Mode mode) = 0;

        /**
         * @brief mode aquire Connection current mode.
         * @return
         */
        virtual Mode mode() const = 0;

        /**
         * @brief readWouldBlock in blocking mode always return true. In non-blocking mode return true if last read
         * operation would block, otherwise false
         * @return
         */
        virtual bool readWouldBlock() const = 0;

        /**
         * @brief writeWouldBlock in blocking mode always return true. In non-blocking mode return true if last write
         * operation would block, otherwise false
         * @return
         */
        virtual bool writeWouldBlock() const = 0;

     protected:
        Connection() = default;

     private:
        Connection(Connection const&) = delete;
        Connection& operator=(Connection const&) = delete;
    };

    /**
     * @brief The Acceptor class is abstract interface for incomming TCP/IP connections acceptor (used in `servers`).
     */
    class Acceptor {
     public:
        /**
         * @brief The Error class is used when acceptor have internal failure
         */
        class Error : public std::system_error {
         public:
            /**
             * @brief Error cinstruct error based on OS accept function errno
             * @param ec
             */
            explicit inline Error(std::error_code ec)
                : std::system_error(ec, "rili::service::SocketBase::Acceptor::Error") {}
        };
        virtual ~Acceptor() = default;

     public:
        /**
         * @brief accept try accept new incoming connection
         * @return
         */
        virtual std::unique_ptr<Connection> accept() = 0;

     protected:
        Acceptor() = default;

     private:
        Acceptor(Acceptor const& other) = delete;
        Acceptor& operator=(Acceptor const& other) = delete;
    };

 public:
    SocketBase() = default;
    virtual ~SocketBase() = default;

 private:
    SocketBase(SocketBase const& other) = delete;
    SocketBase& operator=(SocketBase const& other) = delete;

 public:
    /**
     * @brief createConnection try create TCP/IP connection to given host and port
     * @param host
     * @param port
     * @return new Connection object
     *
     * @note this function might throw in case of failures
     */
    virtual std::unique_ptr<Connection> createConnection(std::string const& host, std::uint16_t port) = 0;

    /**
     * @brief createConnection use already connected TCP/IP socket to create Connection instance
     * @param connectedSocket must be connected, in non blocking mode and valid raw socket descriptor
     * @return new Connection object
     */
    virtual std::unique_ptr<Connection> createConnection(int connectedSocket) = 0;

    /**
     * @brief createAcceptor try create TCP/IP acceptor which will be able to accept incoming connections on given host
     * and port
     * @param host
     * @param port
     * @return new acceptor object
     *
     * @note this function might throw in case of failures
     */
    virtual std::unique_ptr<Acceptor> createAcceptor(std::string const& host, std::uint16_t port) = 0;
};

/**
 * @brief The Socket class is default, singleton implementation of SocketBase
 */
class Socket : public SocketBase {
 public:
    /**
     * @brief instance is used to access instance of Socket.
     * @return instance of Socket.
     */
    static SocketBase& instance();

    std::unique_ptr<Connection> createConnection(std::string const& host, std::uint16_t port) override;
    std::unique_ptr<Connection> createConnection(int connectedSocket) override;
    std::unique_ptr<Acceptor> createAcceptor(std::string const& host, std::uint16_t port) override;

 public:
    ~Socket();

 private:
    Socket(Socket const& other) = delete;
    Socket& operator=(Socket const& other) = delete;
    Socket();
};
}  // namespace service
}  // namespace rili
